package danch.barter.math

import danch.barter.math.Factories.{Matrix, forEach}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

class MathUtilsSpec  extends AnyWordSpec with Matchers {
  private val TEST_3X3_ARRAY = Matrix(Array(
    Array(1.0d, 2.0d, 3.0d),
    Array(1.0d, 2.0d, 3.0d),
    Array(1.0d, 2.0d, 3.0d)
  ))
  private val MATRIX_TWO = Matrix(Array(
    Array(2.0d, 2.0d, 2.0d),
    Array(2.0d, 2.0d, 2.0d),
    Array(2.0d, 2.0d, 2.0d)
  ))

  "scalarPower" must {
    "exponentiate each entry of a matrix by its corresponding entry in a second" in {
      val result = MathUtils.scalarPower(TEST_3X3_ARRAY, 2.0d)
      forEach(result, (value, row, col) => assert(value == math.pow(TEST_3X3_ARRAY.getEntry(row, col), 2.0d) ) )
    }
  }
  "sumRows" must {
    "sum rows" in {
      val result = MathUtils.sumRows(MATRIX_TWO)
      assert(result.length == MATRIX_TWO.getRowDimension)
      for (value <- result) assert(value == 6.0d)
    }
  }
}
