package danch.barter.math

import danch.barter.math.Factories.{Matrix, forEach}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

abstract class UtilityFunctionSpecs extends AnyWordSpec with Matchers {
  private val MATRIX_TWO = Matrix(Array(
    Array(2.0d, 2.0d, 2.0d),
    Array(2.0d, 2.0d, 2.0d),
    Array(2.0d, 2.0d, 2.0d)
  ))
  private val MATRIX_COLUMN_ONE = Matrix(Array(
    Array(1.0d, 0.0d, 0.0d),
    Array(1.0d, 0.0d, 0.0d),
    Array(1.0d, 0.0d, 0.0d)
  ))
  private val TEST_COEFFICIENTS = Matrix(Array(
    Array(0.7d, 0.2d, 0.1d),
    Array(0.7d, 0.2d, 0.1d),
    Array(0.7d, 0.2d, 0.1d),
  ))
  private val TEST_COEFFICIENTS2 = Matrix(Array(
    Array(0.7d, 0.2d, 0.1d),
    Array(1.0d, 0.0d, 0.0d),
    Array(0.0d, 0.0d, 0.0d),
  ))

  def getImplementation: UtilityFunction = ???

  "CESUtility" must {
    "calculate a utility per row of matching matrices" in {
      val (result, _) = getImplementation(MATRIX_TWO, TEST_COEFFICIENTS)
      assert(result.length == MATRIX_TWO.getRowDimension)
      assert(result(0)==result(1))
      assert(result(0)==result(2))
    }
    "be covariant with volume" in {
      val (smaller, _) = getImplementation(MATRIX_TWO, TEST_COEFFICIENTS)
      val moreVolume = MATRIX_TWO.power(2)
      val (larger, _) = CESUtility(moreVolume, TEST_COEFFICIENTS)
      val both = smaller.zip(larger)
      both.foreach(pair => assert(pair._1 < pair._2))
    }
    "be covariant with coefficients" in {
      val (_, breakdown) = getImplementation(MATRIX_TWO, TEST_COEFFICIENTS2)
      assert(breakdown.getEntry(0, 0) > breakdown.getEntry(0, 1))
    }
  }
}
