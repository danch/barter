package danch.barter.math

import org.apache.commons.math3.linear.MatrixUtils
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

class MatrixMapperSpec extends AnyWordSpec with Matchers {
  "MatrixMapper" must {
    "reduce an identity matrix" in {
      val matrixMapper = MatrixMapper(2, 2, 2)
      matrixMapper.addMap(0, 0, 0)
      matrixMapper.addMap(1, 1, 1)

      val identityMatrix = MatrixUtils.createRealIdentityMatrix(2)

      val asVector =  matrixMapper.convert(identityMatrix)
      assert(asVector.length == 2)
      assert(asVector(0) == 1.0d)
      assert(asVector(1) == 1.0d)

      val backToMatrix = matrixMapper.convert(asVector)

      val origData = identityMatrix.getData
      val retData = backToMatrix.getData
      for (i <- 0 until backToMatrix.getRowDimension) {
        for (j <- 0 until backToMatrix.getColumnDimension) {
          assert(origData(i)(j) == retData(i)(j))
        }
      }
    }
    "convert between squished and expanded arrays" in {
      val matrixMapper = MatrixMapper(2, 3, 3)
      matrixMapper.addMap(0, 1, 0)
      matrixMapper.addMap(0,2, 1)
      matrixMapper.addMap(1, 0, 2)

      val fullArray1 = Array(0.0d, 42.0d, 42.0d, 42,0d, 0.0d, 0.0d)
      val squished = matrixMapper.squishArray(fullArray1)
      assert(squished.sum == 126.0d)
    }
  }
}
