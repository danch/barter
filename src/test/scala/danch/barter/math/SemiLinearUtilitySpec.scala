package danch.barter.math

import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

class SemiLinearUtilitySpec extends UtilityFunctionSpecs {
  override def getImplementation: UtilityFunction = new SemiLinearUtility(0.9, 1.0)
}
