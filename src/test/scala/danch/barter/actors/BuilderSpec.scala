package danch.barter.actors

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import danch.barter.actors.Builder.RecipeBuildCommand
import danch.barter.models.simple.Agrarian.{BreadTag, WheatTag}
import danch.barter.messages.{AdjustInventoryCommand, EconomicEntityProtocol, Recipe}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.reflect.ClassTag

class BuilderSpec extends AnyWordSpec with BeforeAndAfterAll with Matchers {
  private val testKit = ActorTestKit()

  override def afterAll(): Unit = testKit.shutdownTestKit()

  private val WHEAT_PER = 100.0d
  private val BREAD_PER = 30.0d
  private val recipe = Recipe().using(WHEAT_PER, WheatTag).making(BREAD_PER, BreadTag)

  "A Builder" must {
    "Produce an AdjustInventoryMessage in response to build commands" in {
      val probe = testKit.createTestProbe[EconomicEntityProtocol]()

      val aut = testKit.spawn(Builder())

      aut ! RecipeBuildCommand(recipe, Map(WheatTag -> WHEAT_PER), probe.ref)

      val response: AdjustInventoryCommand = probe.expectMessageType(ClassTag(classOf[AdjustInventoryCommand]))

      assert(response.deltas.getOrElse(BreadTag, 0.0d) == BREAD_PER)
    }
    "Return excess materieals" in {
      val probe = testKit.createTestProbe[EconomicEntityProtocol]()

      val aut = testKit.spawn(Builder())

      aut ! RecipeBuildCommand(recipe, Map(WheatTag -> (WHEAT_PER + 20.0d)), probe.ref)

      val response: AdjustInventoryCommand = probe.expectMessageType(ClassTag(classOf[AdjustInventoryCommand]))

      assert(response.deltas.getOrElse(WheatTag, 0.0d) == 20.0d)
      assert(response.deltas.getOrElse(BreadTag, 0.0d) == BREAD_PER)
    }
    "reduce output if not enough materials" in {
      val probe = testKit.createTestProbe[EconomicEntityProtocol]()

      val aut = testKit.spawn(Builder())

      aut ! RecipeBuildCommand(recipe, Map(WheatTag -> (WHEAT_PER - 20.0d)), probe.ref)

      val response: AdjustInventoryCommand = probe.expectMessageType(ClassTag(classOf[AdjustInventoryCommand]))

      assert(response.deltas.getOrElse(WheatTag, 0.0d) == 0.0d)
      assert(response.deltas.getOrElse(BreadTag, 0.0d) < BREAD_PER)
    }
  }
}
