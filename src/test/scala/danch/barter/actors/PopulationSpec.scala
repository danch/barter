package danch.barter.actors

import akka.actor.testkit.typed.scaladsl.{ActorTestKit, BehaviorTestKit}
import danch.barter.models.simple.Agrarian.WheatTag
import danch.barter.messages.{AdjustInventoryCommand, AdjustPopulationCommand, CommonScaffold, ConsumptionEvent, EatCommand, EconomicEvent, Identity, SetPreferenceCommand, TradeCommand}
import danch.barter.messages.MarketProtocol.{MarketCommand, RegisterTradeCommand}
import danch.barter.models.PopulationConfig
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.reflect.ClassTag

class PopulationSpec extends AnyWordSpec with BeforeAndAfterAll with Matchers with CommonScaffold {
  private val testKit = ActorTestKit()

  override def afterAll(): Unit = testKit.shutdownTestKit()

  "determineUtilityCoefficients" must {
    "Create reasonable coefficients" in {
      val coefficients = Population.determineUtilityCoefficients(simplePreference)

      assert(coefficients.keySet.size == 3)
      assert(coefficients.values.sum == 1.0d)
    }
  }
  "determineTrades" must {
    "create a default trade" in {
      val trades = Population.determineTrades(Population.State(Identity(null, "test"), 1000.0d, Some(simplePreference), Map()))

      assert(trades.isDefined)
      assert(trades.get.proposal.size == 3.0)
      trades.map(trades => {
        val paired = trades.proposal.map(pair => (pair._1, (pair._2, trades.constraints.get(pair._1))))
        val penalties = paired.map(pair => pair._2._2.map(constraint => constraint.calculatePenalty(pair._2._1)))
        assert(penalties.map(opt => opt.getOrElse(1.0)).max == 0.0d)
      })
    }
    "Create sell trades" in {
      val trades = Population.determineTrades(Population.State(Identity(null, "test"), 1000.0d, Some(simplePreference), Map(WheatTag -> 500.0)))

      val optSells = trades.map(vp => vp.proposal.filter(pair => pair._2 < 0.0d))
      assert(optSells.nonEmpty)
    }
  }
  "The Population Actor" must {
    "generate a trade message on command" in {
      val marketProbe = testKit.createTestProbe[MarketCommand]()
      val eventProbe = testKit.createTestProbe[EconomicEvent]()
      val populationConfig = PopulationConfig(1000.0d, Map(), Map(), simplePreference)
      val population = testKit.spawn(Population(populationConfig, marketProbe.ref, eventProbe.ref, None, "pop"))

      population ! TradeCommand
      val tradeCommand: RegisterTradeCommand = marketProbe.expectMessageType(wait_duration)( ClassTag(classOf[RegisterTradeCommand]) )
      assert(tradeCommand.proposal.nonEmpty)
      assert(tradeCommand.proposal(aTag) > 0.0d)
    }
    "not sell something it consumes" in {
      val marketProbe = testKit.createTestProbe[MarketCommand]()
      val eventProbe = testKit.createTestProbe[EconomicEvent]()
      val populationConfig = PopulationConfig(1000.0d, Map(aTag -> 100.0d), Map(), simplePreference)
      val population = testKit.spawn(Population(populationConfig, marketProbe.ref, eventProbe.ref, None, "pop"))

      population ! TradeCommand
      val tradeCommand: RegisterTradeCommand = marketProbe.expectMessageType(wait_duration)( ClassTag(classOf[RegisterTradeCommand]) )
      assert(tradeCommand.proposal.nonEmpty)
      assert(tradeCommand.proposal(aTag) > 100.0d)
    }
    "sell things it doesn't consume" in {
      val marketProbe = testKit.createTestProbe[MarketCommand]()
      val eventProbe = testKit.createTestProbe[EconomicEvent]()
      val populationConfig = PopulationConfig(1000.0d, Map(LaborTag -> 100.0d), Map(), simplePreference)
      val population = testKit.spawn(Population(populationConfig, marketProbe.ref, eventProbe.ref, None, "pop"))

      population ! TradeCommand
      val tradeCommand: RegisterTradeCommand = marketProbe.expectMessageType(wait_duration)( ClassTag(classOf[RegisterTradeCommand]) )
      assert(tradeCommand.proposal.nonEmpty)
      assert(tradeCommand.proposal(LaborTag) < 0.0d)
    }
    "consume commodities" in {
      val marketProbe = testKit.createTestProbe[MarketCommand]()
      val eventProbe = testKit.createTestProbe[EconomicEvent]()
      val populationConfig = PopulationConfig(1000.0d, Map(LaborTag -> 100.0d, bTag -> 2000.0d, aTag -> 1500.0d), Map(), simplePreference)
      val population = testKit.spawn(Population(populationConfig, marketProbe.ref, eventProbe.ref, None, "pop"))

      population ! AdjustPopulationCommand(1000.0d)
      population ! SetPreferenceCommand(simplePreference)
      population ! AdjustInventoryCommand(Map(LaborTag -> 1000.0d, bTag -> 2000.0d, aTag -> 1500.0d))
      population ! EatCommand

      val ce: ConsumptionEvent = eventProbe.expectMessageType(wait_duration)(ClassTag(classOf[ConsumptionEvent]))
      assert(ce.consumption.get(aTag).map(v => (v < 1500.0d)).getOrElse(false))
      assert(ce.consumption.get(bTag).map(v => (v < 2000.0d)).getOrElse(false))
    }
  }
}
