package danch.barter.actors

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import danch.barter.models.simple.Agrarian.{BreadTag, WheatTag}
import danch.barter.messages.MarketProtocol.{MarketCommand, RegisterTradeCommand, TradeExecutedEvent}
import danch.barter.messages.{AddRecipeCommand, AdjustInventoryCommand, CommonScaffold, InventoryQuery, InventoryQueryResponse, Recipe, TradeCommand}
import danch.barter.models.ManufactoriumConfig
import danch.barter.models.simple.Agrarian
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.reflect.ClassTag

class ManufactoriumSpec extends AnyWordSpec with BeforeAndAfterAll with Matchers with CommonScaffold {
  private val testKit = ActorTestKit()

  override def afterAll(): Unit = testKit.shutdownTestKit()

  private val WHEAT_PER = 100.0d
  private val BREAD_PER = 30.0d
  private val recipe = Recipe().using(WHEAT_PER, WheatTag).making(BREAD_PER, BreadTag)

  "A Manufactorium" must {
    "Request appropriate ingredients" in {
      val probe = testKit.createTestProbe[MarketCommand]()

      val config = ManufactoriumConfig("testmfg", List(recipe), Map())
      val aut = testKit.spawn(Manufactorium(Agrarian, config, probe.ref, None))

      aut ! TradeCommand

      val message: RegisterTradeCommand = probe.expectMessageType(wait_duration)( ClassTag(classOf[RegisterTradeCommand]))

      assert(message.proposal(WheatTag) > WHEAT_PER)
    }
    "Accept Inventory Adjustments (Smoke test)" in {
      val probe = testKit.createTestProbe[MarketCommand]()
      val config = ManufactoriumConfig("testmfg", List(recipe), Map())
      val aut = testKit.spawn(Manufactorium(Agrarian, config, probe.ref, None))

      aut ! AdjustInventoryCommand(Map(WheatTag -> 200.0d))
    }
    "Request less resource when having some in inventory" in {
      val probe = testKit.createTestProbe[MarketCommand]()
      val config = ManufactoriumConfig("testmfg", List(recipe), Map())
      val aut = testKit.spawn(Manufactorium(Agrarian, config, probe.ref, None))

      aut ! AddRecipeCommand(recipe)

      aut ! TradeCommand

      val empty: RegisterTradeCommand = probe.expectMessageType(wait_duration)( ClassTag(classOf[RegisterTradeCommand]))

      aut ! AdjustInventoryCommand(Map(WheatTag -> 100.0d))

      aut ! TradeCommand

      val one_third: RegisterTradeCommand = probe.expectMessageType(wait_duration)( ClassTag(classOf[RegisterTradeCommand]))

      assert(empty.proposal(WheatTag) > one_third.proposal(WheatTag))
    }
    "receive inventory at indicated actorref" in {

      val probe = testKit.createTestProbe[MarketCommand]()
      val config = ManufactoriumConfig("testmfg", List(recipe), Map())
      val aut = testKit.spawn(Manufactorium(Agrarian, config, probe.ref, None))

      aut ! TradeCommand

      Thread.sleep(200L)
      val empty: RegisterTradeCommand = probe.expectMessageType(wait_duration)( ClassTag(classOf[RegisterTradeCommand]))

      empty.replyTo ! TradeExecutedEvent(Map(WheatTag -> 100.0d))

      val inventoryProbe = testKit.createTestProbe[InventoryQueryResponse]()

      aut ! InventoryQuery(inventoryProbe.ref)

      val invResponse: InventoryQueryResponse = inventoryProbe.expectMessageType(wait_duration)(ClassTag(classOf[InventoryQueryResponse]))

      assert(invResponse.inventory(WheatTag) == 100.0d)
    }
  }
}