package danch.barter.actors

import akka.actor.testkit.typed.Effect.{Spawned, TimerScheduled}
import akka.actor.testkit.typed.scaladsl.{ActorTestKit, BehaviorTestKit}
import danch.barter.math.{MatrixMapper, SemiLinearUtility}
import danch.barter.models.simple.Agrarian.{BreadTag, WheatTag}
import danch.barter.messages.Constraints.{AtLeast, Between}
import danch.barter.messages.Identity
import danch.barter.messages.MarketProtocol.{ExecuteTradesCommand, MarketEvent, RegisterTradeCommand, TradeExecutedEvent}
import danch.barter.models.simple.Agrarian
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.reflect.ClassTag

class MarketSpec extends AnyWordSpec with BeforeAndAfterAll with Matchers {
  private val testKit = ActorTestKit()
  private val constraints = Map(0 -> Map(BreadTag -> AtLeast(50.0d), WheatTag -> Between(-120.0d, -80.0d)),
                                1 -> Map(BreadTag -> AtLeast(-60.0), WheatTag -> AtLeast(100.0d)))
  private val utilityCoefficients = Array(0.9d, 0.1d, 0.0d, 0.1d, 0.6d, 0.0d)
  private val utilityFunctions = IndexedSeq(new SemiLinearUtility(0.333d, 1.0d), new SemiLinearUtility(0.333d, 1.0d))

  override def afterAll(): Unit = testKit.shutdownTestKit()

  "Market Actor" must {
    "Spawn optimizer" in {
      val synchTestKit = BehaviorTestKit(Market(Agrarian))
      //TODO figure out how to make this work
      //synchTestKit.expectEffect(Spawned(TradeOptimizer.optimizer(Agrarian), "optimizer"))
    }
    "Optimize and execute trades" in {
      val synchTestKit = BehaviorTestKit(Market(Agrarian))

      val aut = testKit.spawn(Market(Agrarian))

      val probe1 = testKit.createTestProbe[MarketEvent]()
      val register1 = RegisterTradeCommand(Identity(Option.empty, "Farm"), Map((BreadTag -> 60.0d), WheatTag -> -100.0d), constraints(0),
                                           Map(BreadTag -> 0.9d, WheatTag -> 0.1d), probe1.ref, utilityFunctions(0))
      aut ! register1

      val probe2 = testKit.createTestProbe[MarketEvent]()
      val register2 = RegisterTradeCommand(Identity(Option.empty, "Bakery"), Map((BreadTag -> -50.0d), WheatTag -> 100.0d), constraints(1),
        Map(BreadTag -> 0.1d, WheatTag -> 0.6d), probe2.ref, utilityFunctions(1))
      aut ! register2

      aut ! ExecuteTradesCommand

      val event1 = probe1.receiveMessage()
      event1 match {
        case tradeExecuted: TradeExecutedEvent =>
          System.out.println(tradeExecuted)
        case _ =>
          fail("received a non TradeExecutedEvent")
      }

      val event2 = probe2.receiveMessage()
      event2 match {
        case tradeExecuted: TradeExecutedEvent =>
          System.out.println(tradeExecuted)
        case _ =>
          fail("received a non TradeExecutedEvent")
      }
    }
  }
  "Optimizer" must {
    "Spew coordinate values" in {
      val matrixMapper = MatrixMapper.unityMap(2, 3)

      var volumes = Array(60.0d, -100.0d, 0.0d, -60.0d, 100.0d, 0.0d)
      val first = TradeOptimizer.totalUtility(utilityCoefficients, constraints, utilityFunctions, matrixMapper, 3, 2)(volumes)
      System.out.println(s"TotalUtility $first at ${volumes(0)}, ${volumes(1)}, ${volumes(2)}, ${volumes(3)}")

      volumes = Array(50.0d, -100.0d, 0.0d, -50.0d, 100.0d, 0.0d)
      val second = TradeOptimizer.totalUtility(utilityCoefficients, constraints, utilityFunctions, matrixMapper, 3, 2)(volumes)
      System.out.println(s"TotalUtility $second at ${volumes(0)}, ${volumes(1)}, ${volumes(2)}, ${volumes(3)}")

      volumes = Array(60.0d, -90.0d, 0.0d, -60.0d, 90.0d, 0.0d)
      val third = TradeOptimizer.totalUtility(utilityCoefficients, constraints, utilityFunctions, matrixMapper, 3, 2)(volumes)
      System.out.println(s"TotalUtility $third at ${volumes(0)}, ${volumes(1)}, ${volumes(2)}, ${volumes(3)}")

      assert(first > second)
      assert(third < 0.0d)//it's not balanced and should be heavily penalized
    }
  }
  "normalizeColumns" must {
    "normalize unbalanced matrices" in {
      var volumes = Array(10.0d, -40.0d, 10.0d, -5, 30.0d, 0.0d)
      var normalized = TradeOptimizer.normalizeColumns(volumes, 3, 2)
      assert(normalized(0) == 5.0)
      assert(normalized(1) == -30.0)
      assert(normalized(2) == 0.0)
      assert(normalized(3) == -5.0)
      assert(normalized(4) == 30.0)
      assert(normalized(5) == 0.0)
    }
  }
}
