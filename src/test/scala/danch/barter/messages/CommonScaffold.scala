package danch.barter.messages

import danch.barter.messages.Preference.{Between, FarOver, Indifferent, NoMoreThan, Over, Unlimited}

import scala.concurrent.duration.DurationInt

trait CommonScaffold {
  protected val wait_duration = 5.seconds

  protected val tagset = new TagSet("test")

  protected val aTag = tagset("A")
  protected val bTag = tagset("B")
  protected val cTag = tagset("C")
  protected val LaborTag = tagset("Labor")

  protected val cPref = Preference(cTag, Indifferent, None, Preference.AtLeast(1.0d))
  protected val bPref = Preference(bTag, Over, Some(cPref), Between(0.8d, 1.2d))
  protected val simplePreference = Preference(aTag, FarOver, Some(bPref), NoMoreThan(1.2d))
}
