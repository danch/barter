package danch.barter.messages

import danch.barter.messages.Preference.{FarOver, Indifferent, Over}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

class PreferenceSpec extends AnyWordSpec with BeforeAndAfterAll with Matchers with CommonScaffold {

  "preferences" must {
    "chain together" in {
      val offsets = simplePreference.getOffsetList
      assert(offsets.size == 3)
      assert(offsets(0) > offsets(1))
      assert(offsets(1) > offsets(2))
    }
  }
}
