import danch.barter.messages.Tag

case class Variable(series: Tag, subscript: Array[Tag])
