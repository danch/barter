package danch.barter.models

import danch.barter.messages.{Preference, Tag}

case class PopulationConfig(startPopulation: Double, startingInventory: Map[Tag, Double], laborGeneration: Map[Tag, Double], preferences: Preference)
