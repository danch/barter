package danch.barter.models.simple

import danch.barter.messages.Preference.{Between, FarOver, Indifferent, NoMoreThan, Over, Unlimited}
import danch.barter.messages.{Preference, Recipe, Tag, TagSet}
import danch.barter.models.{ManufactoriumConfig, Model, PopulationConfig}

object Agrarian extends Model {
  private val commodityTagSet = new TagSet("commodities")

  def commodityCount: Int = {
    commodityTagSet.cardinality()
  }

  def allCommodities: Seq[Tag] = {
    commodityTagSet.allTags()
  }

  def commodity(i: Int): Tag = {
    commodityTagSet(i)
  }

  val BreadTag: Tag = commodityTagSet("Bread")
  val WheatTag: Tag = commodityTagSet("Wheat")
  val LaborTag: Tag = commodityTagSet("Labor")
  val BeerTag: Tag = commodityTagSet("Beer")

  def laborTag: Tag = {
    LaborTag
  }

  override def populationConfig: Option[PopulationConfig] = None

  private val laborRecipe = Recipe().using(200.0, BreadTag).using(50.0, BeerTag).making(800.0, LaborTag)
  private val breadRecipe = Recipe().using(200.0d, WheatTag).using(200.0, LaborTag).making(200.0d, BreadTag)
  private val beerRecipe = Recipe().using(100.0d, WheatTag).using(200.0, LaborTag).making(40.0d, BeerTag)
  private val wheatRecipe = Recipe().using(300.0d, LaborTag).making(300.0d, WheatTag)
  override def factoryConfigs: List[ManufactoriumConfig] = List(
    ManufactoriumConfig("farm", List(wheatRecipe), Map(WheatTag -> 300.0)),
    ManufactoriumConfig("bakery", List(breadRecipe), Map(WheatTag -> 100.0, BreadTag -> 200.0)),
    ManufactoriumConfig("brewery", List(beerRecipe), Map(WheatTag -> 100.0, BeerTag -> 40.0)),
    ManufactoriumConfig("populatioin", List(laborRecipe), Map(LaborTag -> 800.0))
  )


  private val seriesTagSet = new TagSet("series")
  val tradeVolume: Tag = seriesTagSet("volume")
  val Volume: Tag = seriesTagSet("volume")

  private val indexSet = new TagSet("indices")
  val partyIndex: Tag = indexSet("party")
  val productIndex: Tag = indexSet("product")
}
