package danch.barter.models

import danch.barter.messages.{Recipe, Tag}

case class ManufactoriumConfig(baseName: String, recipes: List[Recipe], startInventory: Map[Tag, Double])
