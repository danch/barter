package danch.barter.models

import danch.barter.messages.Tag

trait Model {
  def commodityCount: Int
  def allCommodities: Seq[Tag]
  def commodity(i: Int) : Tag

  def laborTag: Tag

  def populationConfig: Option[PopulationConfig]
  def factoryConfigs: List[ManufactoriumConfig]
}
