package danch.barter.messages

import danch.barter.messages.Constraints.{Constraint, Unconstrained}
import danch.barter.messages.Preference.{Relation, ScalingConstraint}

import scala.language.implicitConversions


object Preference {
  private val STRONG_INCREMENT = 1.0d
  private val WEAK_INCREMENT = 0.25d

  sealed trait Relation {
    def increment: Double
  }
  case object FarOver extends Relation {
    override def increment: Double = {STRONG_INCREMENT}
  }
  case object Over extends Relation {
    override def increment: Double = {WEAK_INCREMENT}
  }
  case object Indifferent extends Relation {
    override def increment: Double = {0.0d}
  }

  implicit def tagToPreference(tag: Tag) : Preference = {
    Preference(tag, Indifferent, None, Unlimited())
  }

  sealed trait ScalingConstraint {
    def toConstraint(scale: Double) : Constraint
  }
  final case class Unlimited() extends ScalingConstraint {
    override def toConstraint(scale: Double): Constraint = {
      Unconstrained()
    }
  }
  final case class Between(min: Double, max: Double) extends ScalingConstraint {
    override def toConstraint(scale: Double): Constraint = {
      Constraints.Between(min * scale, max * scale)
    }
  }
  final case class NoMoreThan(max: Double) extends ScalingConstraint {
    override def toConstraint(scale: Double): Constraint = {
      Constraints.NoMoreThan(max * scale)
    }
  }
  final case class AtLeast(min: Double) extends ScalingConstraint {
    override def toConstraint(scale: Double): Constraint = {
      Constraints.AtLeast(min * scale)
    }
  }
}
case class Preference(left: Tag, relation: Relation, right: Option[Preference], constraint: ScalingConstraint) {
  def getTagList: List[Tag] = {
    List(left) ++ right.map(p => p.getTagList).getOrElse(List())
  }
  def getOffsetList: List[Double] = {
    List(relation.increment) ++ right.map(p => p.getOffsetList).getOrElse(List())
  }
  def getConstraintList: List[ScalingConstraint] = {
    List(constraint) ++ right.map(p => p.getConstraintList).getOrElse(List())
  }
  def map[B](f: Preference => B) : List[B] = {
    val foo = right.map(pref => {pref.map(f)}).getOrElse(List())
    List( f(this) ) ++ foo
  }
  def scale(by: Double) : List[(Tag, Constraint)] = {
    val rightScaled = right.map(pref => pref.scale(by)).getOrElse(List())
    List((left, constraint.toConstraint(by))) ++ rightScaled
  }
}
