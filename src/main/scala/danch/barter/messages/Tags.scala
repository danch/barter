package danch.barter.messages


// Really just an interned string
case class Tag(name: String, index: Int, tagSet: TagSet) {
  def cardinality: Int = tagSet.cardinality()

  override def toString: String = {
    name;
  }
}

class TagSet(setName: String) {

  //TODO tag's native format should be an array, in .index order
  private var tagSet: Map[String, Tag] = Map.empty
  def apply(n: String): Tag = {
    if (!(tagSet contains n)) {
      tagSet += (n -> Tag(n, tagSet.size, this))
    }
    tagSet(n)
  }
  def apply(i: Int): Tag = {
    return tagSet.filter(pair => pair._2.index == i).values.head
  }

  def allTags(): Array[Tag] = {
    Array.from(tagSet.values).sortWith((left, right) => left.index < right.index)
  }

  def cardinality(): Int = tagSet.size

}
