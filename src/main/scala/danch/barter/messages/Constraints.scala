package danch.barter.messages

import danch.barter.math.MathUtils.throwNaN

import scala.math.{abs, pow}

object Constraints {
  protected val CONSTRAINT_ERROR_POWER = 1.5d

  sealed trait Constraint {
    def calculatePenalty(actualVolume: Double): Double
    def default(): Double
  }

  final case class Unconstrained() extends Constraint {
    override def calculatePenalty(actualVolume: Double): Double = 0.0

    override def default(): Double = 0.0
  }

  final case class Between(min: Double, max: Double) extends Constraint {
    override def default(): Double = min

    override def calculatePenalty(actualVolume: Double): Double = {
      if (actualVolume < min) {
        val dif = min - actualVolume
        throwNaN(pow(dif, CONSTRAINT_ERROR_POWER))
      } else {
        if (actualVolume > max) {
          val dif = actualVolume - max
          throwNaN(pow(dif, CONSTRAINT_ERROR_POWER))
        } else {
          0.0d
        }
      }
    }
  }

  final case class NoMoreThan(max: Double) extends Constraint {
    override def default(): Double = max

    override def calculatePenalty(actualVolume: Double): Double = {
      if (actualVolume > max) {
        val dif = max - actualVolume
        throwNaN(pow(math.abs(dif), CONSTRAINT_ERROR_POWER))
      } else {
        0.0d
      }
    }
  }

  final case class AtLeast(min: Double) extends Constraint {
    override def default(): Double = min

    override def calculatePenalty(actualVolume: Double): Double = {
      if (actualVolume < min) {
        val dif = actualVolume - min
        throwNaN(pow(abs(dif), CONSTRAINT_ERROR_POWER))
      } else {
        0.0d
      }
    }
  }

  final case class Exactly(amount: Double) extends Constraint {
    override def default(): Double = amount

    override def calculatePenalty(actualVolume: Double): Double = {
      val diff = actualVolume - amount
      throwNaN(pow(abs(diff), CONSTRAINT_ERROR_POWER))
    }
  }
}