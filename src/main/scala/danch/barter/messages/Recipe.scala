package danch.barter.messages

import scala.collection.immutable.List

object Recipe {
  def apply() : Recipe = {
    Recipe(List(), List())
  }
}

case class Recipe(ingredients: List[(Tag, Double)], output: List[(Tag, Double)]) {
  def using(quantity: Double, item: Tag): Recipe = {
    Recipe( (item, quantity) :: ingredients, output)
  }
  def making(quantity: Double, item: Tag): Recipe = {
    Recipe(ingredients, (item, quantity) :: output)
  }
}
