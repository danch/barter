package danch.barter.messages

import akka.actor.typed.ActorRef
import danch.barter.math.MathUtils.throwNaN
import danch.barter.math.UtilityFunction
import danch.barter.messages.Constraints.Constraint
import danch.barter.messages.Identity

import scala.math.{abs, pow}

case class TradeItem(itemName: Tag, quantityToTrade: Double)

object MarketProtocol {

  sealed trait MarketCommand;
  final case class RegisterTradeCommand(identity: Identity, proposal: Map[Tag, Double], volumeConstraints: Map[Tag, Constraint],
                                        utilityCoefficients: Map[Tag, Double], replyTo: ActorRef[MarketEvent],
                                        utilityFunction: UtilityFunction) extends MarketCommand
  final case object ExecuteTradesCommand extends MarketCommand

  sealed trait MarketEvent
  final case class TradeExecutedEvent(adjustments: Map[Tag, Double]) extends MarketEvent
}
