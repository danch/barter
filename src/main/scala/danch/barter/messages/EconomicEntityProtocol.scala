package danch.barter.messages

import akka.actor.typed.ActorRef

// At least some of these commands will need a credential for security purposes. Others should
//  have some mechanism preventing their being created by unauthorized code (i.e. EazCommand should
//  only be sent by timers in the actors package or some such restriction)
sealed trait EconomicEntityProtocol
case class AdjustInventoryCommand(deltas: Map[Tag, Double]) extends EconomicEntityProtocol
case class AddRecipeCommand(recipe: Recipe) extends EconomicEntityProtocol
case class SetUtilityCoefficient(commodity: Tag, coefficient: Double) extends EconomicEntityProtocol
case class SetPreferenceCommand(preference: Preference) extends EconomicEntityProtocol
case class AdjustPopulationCommand(delta: Double) extends EconomicEntityProtocol

case object EatCommand extends EconomicEntityProtocol
case object HarvestCommand extends EconomicEntityProtocol
case object TradeCommand extends EconomicEntityProtocol
case object BuildCommand extends EconomicEntityProtocol

case class InventoryQueryResponse(inventory: Map[Tag, Double])
case class InventoryQuery(replyTo: ActorRef[InventoryQueryResponse]) extends EconomicEntityProtocol

  //No, this is not the long term approach to this. This is laughably bad.

