package danch.barter.messages

sealed trait EconomicEvent
case class ConsumptionEvent(population: Identity, consumption: Map[Tag, Double]) extends EconomicEvent {

  private def formatConsumption(consumption: Map[Tag, Double]) : String = {
    "\n\t" + consumption.map {case (key, value) => s"${key.name} -> ${value}"}.mkString(", ")
  }

  override def toString: String = s"Consumption by ${population.name}: ${formatConsumption(consumption)}"
}

