package danch.barter.messages

case class Identity(parent: Option[Identity], name: String) {
  override def toString: String = {
    parent.map(p => p.toString).getOrElse("") + "." + name
  }
}
