package danch.barter

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorSystem, Behavior}
import danch.barter.actors.{Locale, Manufactorium, Market}
import danch.barter.models.simple.Agrarian.{BreadTag, WheatTag, breadRecipe, wheatRecipe}
import danch.barter.messages.{AddRecipeCommand, AdjustInventoryCommand, Recipe}
import danch.barter.models.simple.Agrarian

import scala.util.{Failure, Success, Try}

object Main {
  sealed trait RootCommand


  def root(): Behavior[RootCommand] = {
    Behaviors.setup(context => {
      val locale = context.spawn(Locale(Agrarian, None, "root"), "root")

      Behaviors.receive((context, message) => {
        Behaviors.same
      })
    })

  }

  def main(args: Array[String]): Unit = {
    val system = ActorSystem(root(), "hello")

    println("Service started")
    var alive = true
    while (alive) {
      val result = Try { Thread.sleep(1000)}
      result match {
        case Success(_) =>
          alive = true
        case Failure(exc) =>
          println("Exception in sleep: {}", exc)
          alive = false
      }
    }
  }
}
