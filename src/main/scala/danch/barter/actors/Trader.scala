package danch.barter.actors

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import danch.barter.messages.{AdjustInventoryCommand, EconomicEntityProtocol, Tag}
import danch.barter.messages.MarketProtocol.{MarketEvent, TradeExecutedEvent}
import org.slf4j.Logger

/** basically an adapter between EconomicEntityProtocol and MarketProtocol. */
object Trader {
  val log: Logger = org.slf4j.LoggerFactory.getLogger(Trader.getClass)

  def apply(parent: ActorRef[EconomicEntityProtocol]) : Behavior[MarketEvent] = {
    Behaviors.receive((context, message) => {
      message match {
        case TradeExecutedEvent(adjustments) =>
          log.debug(s"Trade received for ${parent.path.toStringWithoutAddress}: ${formatAdjustments(adjustments)}")
          parent ! AdjustInventoryCommand(adjustments)
          Behaviors.stopped
      }
    })
  }

  private def formatAdjustments(adjustments: Map[Tag, Double]) = {
    "\n\t" + adjustments.map {case (key, value) => s"${key.name} -> ${value}"}.mkString(", ")
  }
}