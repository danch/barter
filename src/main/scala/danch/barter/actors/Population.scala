package danch.barter.actors

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import danch.barter.math.{CESUtility, SemiLinearUtility}
import danch.barter.math.Factories.Matrix
import danch.barter.messages.Constraints.{Constraint, NoMoreThan}
import danch.barter.messages.MarketProtocol.{MarketCommand, RegisterTradeCommand}
import danch.barter.messages.{AdjustInventoryCommand, AdjustPopulationCommand, BuildCommand, ConsumptionEvent, EatCommand, EconomicEntityProtocol, EconomicEvent, Identity, Preference, SetPreferenceCommand, Tag, TradeCommand}
import danch.barter.models.{Model, PopulationConfig}
import org.apache.commons.math3.analysis.MultivariateFunction
import org.apache.commons.math3.optim.{InitialGuess, MaxEval, SimpleValueChecker}
import org.apache.commons.math3.optim.nonlinear.scalar.{GoalType, ObjectiveFunction}
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.{NelderMeadSimplex, SimplexOptimizer}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration.DurationInt

object Population {
  val log : Logger = LoggerFactory.getLogger(Population.getClass)


  case class State(identity: Identity, population: Double, preferences: Option[Preference], inventory: Map[Tag, Double]) {
    def addPopulation(delta: Double): State = {
      State(identity, population + delta, preferences, inventory)
    }
    def setPreferences(preference: Preference) : State = {
      State(identity, population, Some(preference), inventory)
    }
    def adjustInventory(deltas: Map[Tag,Double]) : State = {
      val allTags = inventory.keySet ++ deltas.keySet
      val newInventory = Map[Tag, Double]() ++ allTags.map(t => {
        val pre = inventory.getOrElse(t, 0d)
        val delta = deltas.getOrElse(t, 0d)
        t -> (pre + delta)
      })
      log.trace(s"New inventory: $newInventory")
      State(identity, population, preferences, newInventory)
    }
  }

  def apply(config: PopulationConfig, market: ActorRef[MarketCommand], eventHub: ActorRef[EconomicEvent], parent: Option[Identity], name: String) : Behavior[EconomicEntityProtocol] = {
    receiver(config, market, eventHub, State(Identity(parent, name), config.startPopulation, Some(config.preferences), config.startingInventory))
  }

  def determineUtilityCoefficients(preferences: Preference): Map[Tag, Double] = {
    var accum = 0.0d
    val proportions = {for (v <- preferences.getOffsetList.reverse) yield {
      accum = accum + v
      accum
    }}.reverse
    val total = proportions.sum
    val coefficents = proportions.map(v => v / total)
    preferences.getTagList.zip(coefficents).toMap
  }

  case class VolumeParameters(proposal: Map[Tag, Double], constraints: Map[Tag, Constraint])
  def determineTrades(state: State) : Option[VolumeParameters] = {
    val constraints = state.preferences.map(_.getConstraintList).map(list => list.zip( state.preferences.map(p => p.getTagList).getOrElse(List()) ))
    constraints.map(c => {
      //NOTE: scaling linearly by population is very likely to, well, scale weirdly.
      val scaledConstraints = c.map(sc => (sc._2, sc._1.toConstraint(state.population)))
      val buys = scaledConstraints.map(pair => (pair._1, pair._2.default())).toMap
      val sells = state.inventory.map(pair => (pair._1, {
        buys.get(pair._1) match {
          case Some(_) =>
            None
          case None =>
            Some(-pair._2)
        }
      })).filter(pair => pair._2.isDefined).map(pair => (pair._1, pair._2.getOrElse(0.0d)))
      VolumeParameters(sells ++ buys, scaledConstraints.toMap)
    })
  }
  private def consume(state: State) : Map[Tag, Double] = {
    state.preferences.map(preferences => {
      val scaled = preferences.scale(state.population)
      val wants = scaled.map { case (tag, constraint) => (tag, constraint.default())}

      val usageConstraints = scaled.toMap

      val startingPoint = wants.map(pair => (pair._1, {
        val available = state.inventory.getOrElse(pair._1, 0.0d)
        math.min(available, pair._2)
      }))
      val limitingConstraints = wants.map(pair => (pair._1, {
        val available = state.inventory.getOrElse(pair._1, 0.0d)
        NoMoreThan(available)
      })).toMap
      val orderedTags = preferences.getTagList.filter(tag => limitingConstraints.contains(tag))
      val coefficients = determineUtilityCoefficients(preferences)
      val coefficientMatrix = Matrix(Array(coefficients.filter(pair => limitingConstraints.contains(pair._1)).values.toArray))

      val semiLinearUtility = new SemiLinearUtility(0.9, 1.0)
      val objective = new ObjectiveFunction(new MultivariateFunction {
        override def value(point: Array[Double]): Double = {
          val (partyUtils, _) = CESUtility(Matrix(Array(point)), coefficientMatrix)
          //calculating the cost of consumed inventory is convenient, but it would probably be more realistic (and expensive)
          // to calculate the utility of remaining inventory
          val (inventoryUtils, _) = semiLinearUtility(Matrix(Array(point)), coefficientMatrix)
          val utility = partyUtils.sum - inventoryUtils.sum // inverse of inventoryUtils of the transaction is the 'opportunity cost'-ish
          val outOfStockPenalty = orderedTags.zipWithIndex.map(pair => limitingConstraints.get(pair._1).map(constraint => constraint.calculatePenalty(point(pair._2))).getOrElse(0.0d)).sum
          val constraintPenalties = orderedTags.zipWithIndex.map(
            pair => usageConstraints.get(pair._1).map(constraint => constraint.calculatePenalty(point(pair._2))).getOrElse(0.0d)
          )
          val result = utility - outOfStockPenalty - constraintPenalties.sum
          log.trace(s"Penalized utility: ${result}. Utility: ${partyUtils.sum}. outOfStockPenalty: ${outOfStockPenalty}. Constraint Penalty: ${constraintPenalties} Inventory Utility Loss: ${inventoryUtils.sum}")
          result
        }
      })

      val optimizer = new SimplexOptimizer(new SimpleValueChecker(0.01, 0.001))
      val guess = new InitialGuess(startingPoint.map(pair => pair._2).toArray);
      val result = optimizer.optimize( objective, new NelderMeadSimplex(startingPoint.size), GoalType.MAXIMIZE, guess, new MaxEval(1000000) );

      log.trace(s"Optimizer results: ${result.getValue} at ${result.getPoint.mkString("Array(", ", ", ")")} ${orderedTags}")

      val consumption = orderedTags.zipWithIndex.map(pair => (pair._1, - result.getPoint()(pair._2)) ).toMap

      consumption
    }).getOrElse(Map[Tag, Double]())
  }

  def determineLaborProduction(population: Double, config: PopulationConfig): Map[Tag, Double] = {
    config.laborGeneration.map {case (laborTag, rate) => (laborTag, population * rate)}
  }

  private def receiver(config: PopulationConfig, market: ActorRef[MarketCommand], eventHub: ActorRef[EconomicEvent], state: State) : Behavior[EconomicEntityProtocol] = {
    Behaviors.receive((context, message) => {
      message match {
        case EatCommand =>
          val consumption = consume(state)
          eventHub ! ConsumptionEvent(state.identity, consumption)
          receiver(config, market, eventHub, state.adjustInventory(consumption))
        case AdjustPopulationCommand(delta) =>
          receiver(config, market, eventHub, state.addPopulation(delta))
        case AdjustInventoryCommand(deltas) =>
          receiver(config, market, eventHub, state.adjustInventory(deltas))
        case SetPreferenceCommand(preference) =>
          receiver(config, market, eventHub, state.setPreferences(preference))
        case TradeCommand =>
          state.preferences.foreach( preferences => {
            val coefficients = determineUtilityCoefficients(preferences)
            val volumeParameters = determineTrades(state)
            val trader = context.spawnAnonymous(Trader(context.self))
            //RegisterTradeCommand should really accept the whole pile at once\
            volumeParameters.foreach(vp => market ! RegisterTradeCommand(state.identity, vp.proposal, vp.constraints,
                                                                         coefficients, trader, CESUtility))
          })
          Behaviors.same
        case BuildCommand =>
          receiver(config, market, eventHub, state.adjustInventory( determineLaborProduction(state.population, config) ))
        case other =>
          log.debug(s"Population received unhandled message $other")
          Behaviors.same
      }
    })
  }
}
