package danch.barter.actors

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import danch.barter.actors.TradeOptimizer.OptimizeTrade
import danch.barter.math.Factories.Matrix
import danch.barter.math.{Factories, UtilityFunction}
import danch.barter.messages.Constraints.Constraint
import danch.barter.models.Model
import danch.barter.messages.MarketProtocol.{ExecuteTradesCommand, MarketCommand, RegisterTradeCommand, TradeExecutedEvent}
import danch.barter.messages.{Tag, TradeItem}
import org.apache.commons.math3.analysis.MultivariateFunction
import org.apache.commons.math3.optim.{InitialGuess, MaxEval, SimpleValueChecker}
import org.apache.commons.math3.optim.nonlinear.scalar.{GoalType, ObjectiveFunction}
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.{NelderMeadSimplex, SimplexOptimizer}
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.mutable
import scala.concurrent.duration.DurationInt
import scala.math.{abs, log10, pow}

object Market {

  def apply(model: Model) : Behavior[MarketCommand] = market(model, Map.empty)

  private def market(model: Model, oldTrades: Map[String, RegisterTradeCommand]) : Behavior[MarketCommand] = {
    Behaviors.withTimers(timers =>
      Behaviors.setup(context => {
        var optimizer = context.spawn(TradeOptimizer.optimizer(model), "optimizer")
        receiver(model, oldTrades, optimizer)
      })
    )
  }

  private def receiver(model: Model, oldTrades: Map[String, RegisterTradeCommand], optimizer: ActorRef[OptimizeTrade]): Behavior[MarketCommand] = {
    Behaviors.receive((context, message) => {
      message match {
        case ExecuteTradesCommand =>
          if (oldTrades.size > 1) {
            optimizer ! OptimizeTrade(oldTrades.values.toArray)
            receiver(model, Map(), optimizer)
          } else {
            Behaviors.same
          }
        case trade: RegisterTradeCommand =>
          receiver(model, oldTrades + (trade.identity.toString -> trade), optimizer)

        case _ =>
          context.log.error(s"Unexpected message")
          Behaviors.same
      }
    })
  }
}
