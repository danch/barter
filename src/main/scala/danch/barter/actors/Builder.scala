package danch.barter.actors

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import danch.barter.messages.{AdjustInventoryCommand, EconomicEntityProtocol, Recipe, Tag}

import scala.math

object Builder {
  sealed trait BuilderCommand
  final case class RecipeBuildCommand(recipe: Recipe, allocatedMaterials: Map[Tag, Double], replyTo: ActorRef[EconomicEntityProtocol]) extends BuilderCommand

  def apply(): Behavior[BuilderCommand] = {
    Behaviors.receive( (context, message) => {
      message match {
        case RecipeBuildCommand(recipe, allocatedMaterials, replyTo) =>
          val minRatio = recipe.ingredients.toList.map(pair => allocatedMaterials.getOrElse(pair._1, 0.0d) / pair._2 ).minOption.getOrElse(1.0)

          val remainingMaterials = allocatedMaterials.map(alloc_pair => {
            val consumed = recipe.ingredients.find(ing_pair => ing_pair._1 == alloc_pair._1).map(pair => pair._2).getOrElse(0.0d) * math.min(minRatio, 1.0d)
            val newValue = alloc_pair._2 - (consumed)
            (alloc_pair._1 -> newValue )
          } )

          val production = recipe.output.map(pair => (pair._1 -> (pair._2 * math.min(minRatio, 1.0d) )))
          replyTo ! AdjustInventoryCommand(remainingMaterials ++ production)
      }
      Behaviors.stopped
    })
  }
}
