package danch.barter.actors

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import danch.barter.actors.Builder.RecipeBuildCommand
import danch.barter.math.SemiLinearUtility
import danch.barter.messages.Constraints.{AtLeast, Between, Constraint, Exactly}
import danch.barter.messages.Identity
import danch.barter.messages.MarketProtocol.{MarketCommand, RegisterTradeCommand}
import danch.barter.messages.{AddRecipeCommand, AdjustInventoryCommand, BuildCommand, EconomicEntityProtocol, InventoryQuery, InventoryQueryResponse, Recipe, Tag, TradeCommand}
import danch.barter.models.{ManufactoriumConfig, Model}
import org.slf4j.{Logger, LoggerFactory}


object Manufactorium {
  val log : Logger = LoggerFactory.getLogger(Manufactorium.getClass)

  case class State(identity: Identity, recipes: List[Recipe], inventory: Map[Tag, Double]) {
    def withRecipe(newRecipe: Recipe) : State = {
      val newRecipes = newRecipe :: recipes
      State(identity, newRecipes, inventory)
    }
    def adjustInventory(deltas: Map[Tag,Double]) : State = {
      val allTags = inventory.keySet ++ deltas.keySet
      val newInventory = Map[Tag, Double]() ++ allTags.map(t => {
        val pre = inventory.getOrElse(t, 0d)
        val delta = deltas.getOrElse(t, 0d)
        (t -> (pre + delta))
      })
      log.debug(s" ${identity} New inventory: $newInventory")
      State(identity, recipes, newInventory)
    }
    def setUtilityCoefficient(commodity: Tag, weight: Double) : State = {
      State(identity, recipes, inventory)
    }
  }

  def apply(model: Model, config: ManufactoriumConfig, market: ActorRef[MarketCommand], parent: Option[Identity]) : Behavior[EconomicEntityProtocol] = {
    if (config.recipes.size > 1) {
      throw new IllegalArgumentException(s"Factories currently support only a single recipe (factory baseName ${config.baseName}")
    }
    Behaviors.setup(context => {
      receiver(model, market, State(Identity(parent, config.baseName), config.recipes, config.startInventory) )
    })
  }

  def logistic(x: Double) : Double = {
    1.0d / (1 + Math.exp(-0.5d * (x - 0.5d)))
  }
  case class VolumeParameters(proposal: Map[Tag, Double], constraints: Map[Tag, Constraint])
  def determineTrades(state: State, model: Model, toBuild: Recipe): VolumeParameters = {
    val sells = toBuild.output.map(pair => (pair._1 -> state.inventory.get(pair._1))).
      toMap.filter(p => p._2.isDefined).map(pair => (pair._1 -> pair._2.map(d => -d).getOrElse(0.0)))
    val buys = toBuild.ingredients.map {
      case (tag, fullBatchAmount) => {
        val invAmount = state.inventory.getOrElse(tag, 0.0)
        val target = fullBatchAmount * 3.0d
        val logValue = logistic((target - invAmount) / target)
        (tag -> target * logValue)
      }
    }.toMap
    val sellConstraints = sells.map { case (tag, available) => (tag -> AtLeast(available))} //'available' will be negative
    val buyConstraints = buys.map {case (tag, wanted) => (tag -> Between(0.8 * wanted, 1.2 * wanted))}
    if (sells.keySet.intersect(buys.keySet).nonEmpty) {
      log.warn(s"overlapping buy and sell sets: buyset: ${buys.keySet}\tsellset: ${sells.keySet}")
    }
    val usedTags = sells.keySet ++ buys.keySet
    //null constraint should probably be more like AtLeaast(0.0d), but this'll do for now
    val nullConstraints = model.allCommodities.filter(tag => !usedTags.contains(tag)).map(tag => (tag -> Exactly(0.0d))).toMap
    val constraints = sellConstraints ++ buyConstraints ++ nullConstraints
    VolumeParameters(sells ++ buys, constraints)
  }

  private val INGREDIENT_PORTION = 0.7d
  private val PRODUCT_PORTION = 0.3d
  def determineUtilityCoefficients(model: Model, state: State, recipe: Recipe) : Map[Tag, Double] = {
    val perIngredient = INGREDIENT_PORTION / recipe.ingredients.size
    var coefficients = recipe.ingredients.map { case (tag, _)  => (tag -> perIngredient)}.toMap
    val perProduct = PRODUCT_PORTION / recipe.output.size
    coefficients = coefficients ++ recipe.output.map { case (tag, _) => (tag -> perProduct)}.toMap

    val missingTags = model.allCommodities.filter(tag => coefficients.get(tag).isEmpty)

    coefficients ++ missingTags.map( tag => (tag -> 0.0d)).toMap
  }

  private def allocateMaterials(state: Manufactorium.State, recipe: Recipe) : Map[Tag, Double] = {
    val zipped: Iterable[(Tag, Double, Double)] = for(key <- recipe.ingredients.toMap.keys ++ state.inventory.keys)
      yield (key, recipe.ingredients.toMap.getOrElse(key, 0), state.inventory.getOrElse(key, 0))
    zipped.map(trio => (trio._1, math.min(trio._2, trio._3))).toMap
  }

  val VOLUME_DECAY = 0.9d
  val COEFFICIENT_SCALE = 10.0d
  def receiver(model: Model, market: ActorRef[MarketCommand], state: State) : Behavior[EconomicEntityProtocol] = {
    Behaviors.receive((context, message) => {
      log.trace(message.toString)
      message match {
        case AddRecipeCommand(recipe) =>
          receiver(model, market, state.withRecipe(recipe))
        case AdjustInventoryCommand(deltas) =>
          receiver(model, market, state.adjustInventory(deltas))
        case TradeCommand =>
          if (state.recipes.nonEmpty) {
            val volumeParameters = determineTrades(state, model, state.recipes.head)
            val coefficients = determineUtilityCoefficients(model, state, state.recipes.head)
            val trader = context.spawnAnonymous(Trader(context.self))
            market ! RegisterTradeCommand(state.identity, volumeParameters.proposal, volumeParameters.constraints,
              coefficients, trader, new SemiLinearUtility(VOLUME_DECAY, COEFFICIENT_SCALE))
          }
          Behaviors.same
        case InventoryQuery(replyTo) =>
          replyTo ! InventoryQueryResponse(state.inventory)
          Behaviors.same
        case BuildCommand =>
          if (state.recipes.nonEmpty) {
            val child = context.spawnAnonymous(Builder())
            val recipe = state.recipes.head
            val materials = allocateMaterials(state, recipe)
            child ! RecipeBuildCommand(recipe, materials, context.self)
          }
          Behaviors.same
        case wha =>
          log.debug(s"Unknown message: ${wha}")
          Behaviors.same
      }
    })
  }

}
