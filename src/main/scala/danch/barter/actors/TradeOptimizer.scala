package danch.barter.actors

import akka.actor.typed.scaladsl.Behaviors
import danch.barter.math.Factories.Matrix
import danch.barter.math.{MatrixMapper, UtilityFunction}
import danch.barter.messages.Constraints.Constraint
import danch.barter.messages.MarketProtocol.{RegisterTradeCommand, TradeExecutedEvent}
import danch.barter.messages.Tag
import danch.barter.models.Model
import org.apache.commons.math3.analysis.MultivariateFunction
import org.apache.commons.math3.linear.BlockRealMatrix
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.{NelderMeadSimplex, SimplexOptimizer}
import org.apache.commons.math3.optim.nonlinear.scalar.{GoalType, ObjectiveFunction}
import org.apache.commons.math3.optim.{InitialGuess, MaxEval, SimpleValueChecker}
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.mutable
import scala.math.{abs, pow}

// TODO: set up instance pool and separate thread pool
object TradeOptimizer {
  private val log: Logger = LoggerFactory.getLogger(TradeOptimizer.getClass)

  case class OptimizeTrade(parties: Array[RegisterTradeCommand])

  def totalUtility(utilityCoefficients: Array[Double], constraintsByPartyNo: Map[Int, Map[Tag, Constraint]],
                   utilityFunctions: IndexedSeq[UtilityFunction], matrixMapper: MatrixMapper,
                   commodityCount: Int, parties: Int)(partyVolumes: Array[Double]): Double = {
    val partyUtilities = mutable.Stack[Double]()
    val fullMatrix = matrixMapper.convert(partyVolumes)
    val cfxBlockData = (for (i <- 0 until parties) yield utilityCoefficients.slice(i * commodityCount, (i+1) * commodityCount)).toArray
    val cfxMatrix = new BlockRealMatrix(cfxBlockData)
    for (i <- 0 until parties) {
      val volumesMatrix = fullMatrix.getRowMatrix(i)
      val coefficientsMatrix = cfxMatrix.getRowMatrix(i)
      val (partyUtility, _) = utilityFunctions(i)(volumesMatrix, coefficientsMatrix)
      partyUtilities.push(partyUtility(0))
    }
    val accumulator = partyUtilities.sum
    //constraints from traders
    var penalties = 0.0d
    for (i <- 0 until parties) {
      for (mapEntry <- constraintsByPartyNo(i)) {
        val actual = fullMatrix.getEntry(i, mapEntry._1.index)
        penalties += mapEntry._2.calculatePenalty(actual)
      }
    }
    //error term to keep it balanced-ish
    var entropyViolationPenalty = 0.0d
    for (i <- 0 until commodityCount) {
      val sum = fullMatrix.getColumn(i).sum
      val p = pow(abs(sum), 2.0)
      entropyViolationPenalty += p
    }

    val penalizedUtility = accumulator - (penalties + entropyViolationPenalty)

    log.trace(s"Penalized Score: ${penalizedUtility} Sum: ${accumulator} Penalties: ${penalties} Entropy Penalty: ${entropyViolationPenalty}  ${partyUtilities.mkString("Utilities(", ", ", ")")} ${partyVolumes.mkString("Volumes(", ", ", ")")}")

    penalizedUtility
  }


  //The verbosity of this annoys me, but there are far more important phish to phry
  def normalizeColumns(partyVolumes: Array[Double], columns: Int, rows: Int): Array[Double] = {
    var newVolumes = partyVolumes.clone()
    for (i <- 0 until columns) {
      var positive = 0.0d
      var negative = 0.0d
      for (j <- 0 until rows) {
        val cellValue = partyVolumes((j * columns + i))
        if (cellValue > 0.0) {
          positive = positive + cellValue
        } else {
          negative = negative + cellValue
        }
      }
      var columTotal = positive + negative

      if (columTotal < 0.0d) {
        for (j <- 0 until rows) {
          val rowVolume = partyVolumes((j * columns + i))
          if (rowVolume < 0.0d) {
            val factor = rowVolume / negative
            val adjustment = columTotal * factor
            newVolumes((j * columns + i)) = rowVolume - adjustment
          }
        }
      }
      if (columTotal > 0.0d) {
        for (j <- 0 until rows) {
          val rowVolume = partyVolumes((j * columns + i))
          if (rowVolume > 0.0d) {
            val factor = rowVolume / positive
            val adjustment = columTotal * factor
            newVolumes((j * columns + i)) = rowVolume - adjustment
          }
        }
      }
    }
    newVolumes
  }

  def validateVolumeMatrix(partyVolumes: Array[Double], columns: Int, rows: Int): Unit = {
    for (i <- 0 until columns) {
      var columTotal = (for (j <- 0 until rows) yield partyVolumes((j * columns + i))).sum
      if (abs(columTotal) > 0.001d) {
        log.warn(s"Column $i unbalanced by $columTotal")
      }
    }
  }

  def optimizer(model: Model) = Behaviors.receive[OptimizeTrade]((context, message) => {
    val partyCount = message.parties.length
    val tradeCount = message.parties.map(trade => trade.proposal.size).sum
    val matrixMapper = MatrixMapper(partyCount, model.allCommodities.size, tradeCount)

    val utilityCoefficients = Array.fill[Double](message.parties.length * model.commodityCount)(0)
    for (i <- message.parties.indices) {
      message.parties(i).utilityCoefficients.foreach(pair => {
        utilityCoefficients((model.commodityCount * i) + pair._1.index) = pair._2
      })
    }

    var counter = 0;
    var fullMatrix = Array.fill(message.parties.length * model.commodityCount)(0.0d)
    for (i <- message.parties.indices) {
      message.parties(i).proposal.foreach {
        case (tag, amount) =>
          matrixMapper.addMap(i, tag.index, counter)
          counter = counter + 1
          fullMatrix((model.commodityCount * i) + tag.index) += amount
      }
    }
    fullMatrix = normalizeColumns(fullMatrix, model.commodityCount, message.parties.length)
    //validateVolumeMatrix(fullMatrix, model.commodityCount, message.parties.length)
    val partyVolumes = matrixMapper.squishArray(fullMatrix)

    val constraintsByParty = (0 to message.parties.length).zip(message.parties.map(p => p.volumeConstraints)).toMap

    val utilityFunctions = message.parties.map(rtc => rtc.utilityFunction)
    val curried = totalUtility(utilityCoefficients, constraintsByParty, utilityFunctions, matrixMapper, model.commodityCount, message.parties.length)(_)
    val objective = new ObjectiveFunction(new MultivariateFunction {
      override def value(point: Array[Double]): Double = {
        curried(point)
      }
    })

    val optimizer = new SimplexOptimizer(new SimpleValueChecker(0.001, 0.001))
    val guess = new InitialGuess(partyVolumes)
    val start = System.nanoTime()
    val result = optimizer.optimize(objective, new NelderMeadSimplex(partyVolumes.length, 1000.0d), GoalType.MAXIMIZE, guess, new MaxEval(1000000));
    val end = System.nanoTime()
    log.debug(s"Optimizer results (${(end - start) / 1000000000.0d} s): ${result.getValue} at ${result.getPoint.mkString("Point(", ", ", ")")} starting from ${fullMatrix.mkString("Point(", ", ", ")")}")

    val fullResult = matrixMapper.expandArray(result.getPoint)
    for (i <- message.parties.indices) {
      val replyTo = message.parties(i).replyTo
      var tradedItems = Map[Tag, Double]()
      for (j <- 0 until model.commodityCount) {
        val amount = fullResult((i * model.commodityCount) + j)
        tradedItems += (model.commodity(j) -> amount)
      }
      replyTo ! TradeExecutedEvent(tradedItems)

      //message.parties(i).proposal.foreach(pair => fullResult((model.commodityCount * i) + pair._1.index) += pair._2)
    }

    Behaviors.same
  })

}
