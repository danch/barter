package danch.barter.actors

import akka.NotUsed
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import danch.barter.models.simple.Agrarian.{BreadTag, WheatTag, breadRecipe, wheatRecipe}
import danch.barter.messages.{AddRecipeCommand, AdjustInventoryCommand, BuildCommand, EatCommand, EconomicEntityProtocol, EconomicEvent, Identity, TradeCommand}
import danch.barter.messages.MarketProtocol.{ExecuteTradesCommand, MarketCommand, MarketEvent}
import danch.barter.models.Model
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration.DurationInt

object Locale {
  val log : Logger = LoggerFactory.getLogger(Locale.getClass)

  case class State(identity: Identity, market: ActorRef[MarketCommand], population: Option[ActorRef[EconomicEntityProtocol]],
                   factories: List[ActorRef[EconomicEntityProtocol]]) {
    def allEcoEnts() : List[ActorRef[EconomicEntityProtocol]] = {
      population.map(p => p ::  factories).getOrElse(factories)
    }
  }

  def apply(model: Model, parent: Option[Identity], name: String): Behavior[EconomicEntityProtocol] = {
    locale(model, parent, name)
  }

  private def locale(model: Model, parent: Option[Identity], name: String) : Behavior[EconomicEntityProtocol] = {
    Behaviors.withTimers(timers => {
      Behaviors.setup(context => {
        val identity = Identity(parent, "Locale")

        val market = context.spawn(Market(model), identity.toString+"-market")
        val eventHub = context.spawn(EventHub(), identity.toString+"-events")

        val population = model.populationConfig.map(config => context.spawn(Population(config, market, eventHub, Some(identity), "population"), "population"))

        var factoryCounter = 0;
        val factories = model.factoryConfigs.map(config => {
          factoryCounter = factoryCounter + 1
          context.spawn(Manufactorium(model, config, market, Some(identity)), config.baseName+factoryCounter)
        })

        timers.startTimerAtFixedRate(TradeCommand, 5.seconds, 5.seconds)

        val state = State(identity, market, population, factories)
        receiver(state)
      })
    })
  }

  private def receiver(state: State) : Behavior[EconomicEntityProtocol] = {
    Behaviors.receive((context, message) => {
      message match {
        case TradeCommand =>
          state.market ! ExecuteTradesCommand

          val broadcaster = context.spawnAnonymous(BroadcastTick)
          broadcaster ! TickActors(state.allEcoEnts())

        case m =>
          log.warn("Locale ignoring message "+m.toString)
      }
      Behaviors.same
    })
  }

  private case class TickActors(group: List[ActorRef[EconomicEntityProtocol]])

  private def BroadcastTick : Behavior[TickActors] = {
    Behaviors.receive((context, message) => {
      message.group.foreach(a => a ! BuildCommand)
      message.group.foreach(a => a ! EatCommand)
      message.group.foreach(a => a ! TradeCommand)
      Behaviors.stopped
    })
  }
}

private object EventHub {
  def apply() : Behavior[EconomicEvent] = {
    Behaviors.receive((context, event) => {
      System.out.println(event)
      Behaviors.same
    })
  }
}