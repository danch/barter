package danch.barter.math

import org.apache.commons.math3.linear.RealMatrix

object MathUtils {

  def throwNaN(value: Double) : Double = {
    if (!value.isNaN) {
      if (!value.isInfinite) {
        value
      } else {
        throw new NumberFormatException("Infinite")
      }
    } else {
      throw new NumberFormatException("NaN")//TODO: replace with a bespoke exception
    }
  }

  def scalarPower(A: RealMatrix, power: Double) : RealMatrix = {
    val B = A.copy()
    for (row <- Range(0, A.getRowDimension)) {
      for (column <- Range(0, A.getColumnDimension)) {
        val element = A.getEntry(row, column)
        var value = throwNaN(math.pow(math.abs(element), power))
        if (element < 0.0d) {
          value = - value
        }
        B.setEntry(row, column, value)
      }
    }
    B
  }

  def sumRows(matrix: RealMatrix) : Array[Double] = {
    val foo = for (row <- Range(0, matrix.getRowDimension))
      yield {
        for (column <- Range(0, matrix.getColumnDimension)) yield matrix.getEntry(row, column)
      }.sum
    foo.toArray
  }

  def elementMult(left: RealMatrix, right: RealMatrix) : RealMatrix = {
    val result = left.copy()
    for (row <- Range(0, left.getRowDimension)) {
      for (column <- Range(0, left.getColumnDimension)) {
        val value = throwNaN(left.getEntry(row, column) * right.getEntry(row, column))
        result.setEntry(row, column, value)
      }
    }
    result
  }
}
