package danch.barter.math

import org.apache.commons.math3.linear.{BlockRealMatrix, RealMatrix}

object Factories {
  def Matrix(data: Array[Array[Double]]) : RealMatrix = {
    return new BlockRealMatrix(data)
  }

  def forEach(realMatrix: RealMatrix, f: (Double, Int, Int) => Unit) : Unit = {
    for (row <- Range(0, realMatrix.getRowDimension)) {
      for (col <- Range(0, realMatrix.getColumnDimension)) {
        f(realMatrix.getEntry(row, col), row, col)
      }
    }
  }
}
