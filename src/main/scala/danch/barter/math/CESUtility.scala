package danch.barter.math

import danch.barter.math.MathUtils.{elementMult, scalarPower, sumRows, throwNaN}
import org.apache.commons.math3.linear.RealMatrix


object CESUtility extends UtilityFunction {
  private val ELASTICITY = 5.0
  private val DELTA: Double = (ELASTICITY - 1.0) / ELASTICITY

  def apply(volumes: RealMatrix, coefficients: RealMatrix) : (Array[Double], RealMatrix) = {
    //val power: Formula = new Power(new SumRange(variableAssigner.apply(currentItem.get.getItem)), new Literal(DELTA))
    val power =  scalarPower(volumes, DELTA)
    //precompute the proportioning constant (^1/E to enhance substitution effects
    //val proportion: Double = Math.pow(weights(itemIndex), 1.0 / ELASTICITY)
    val proportion = scalarPower(coefficients, 1.0d / ELASTICITY )
    //val product: Formula = new MathProduct(new Literal(proportion), power)
    val product = elementMult(proportion, power)

    val perParty = sumRows(product)
    (perParty, product)
  }
}
