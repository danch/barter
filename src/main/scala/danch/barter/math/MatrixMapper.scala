package danch.barter.math

import org.apache.commons.math3.linear.{BlockRealMatrix, RealMatrix}

trait MatrixMapper {
  def addMap(row: Int, col: Int, arrayIndex: Int)
  def convert(fromArray: Array[Double]) : RealMatrix
  def convert(fromMatrix: RealMatrix) : Array[Double]
  /** convert a fully expanded Array(M*N) (i.e. flattened matrix) into a sparse array */
  def squishArray(fromArray: Array[Double]) : Array[Double]
  /** convert a squished array into a fully expanded one */
  def expandArray(squished: Array[Double]) : Array[Double]
}

object MatrixMapper {
  def apply(rows: Int, columns: Int, vectorSize: Int) : MatrixMapper = {
    new MatrixMapperImpl(rows, columns, vectorSize)
    new MatrixMapperImpl(rows, columns, vectorSize)
  }
  def unityMap(rows: Int, columns: Int) = {
    new UnityMatrixMap(rows, columns)
  }
}


/** primarily an adapter to for some older unit tests that assumed full matrices */
class UnityMatrixMap(rows: Int, columns: Int) extends MatrixMapper {
  val vectorSize: Int = rows * columns

  override def addMap(row: Int, col: Int, arrayIndex: Int): Unit = {}

  override def convert(fromArray: Array[Double]): RealMatrix = {
    val rawData = (for (i <- 0 to rows) yield fromArray.slice(i * columns, (i+1) * columns)).toArray

    new BlockRealMatrix(rawData)
  }

  override def convert(fromMatrix: RealMatrix): Array[Double] = {
    val rawData = fromMatrix.getData()

    var fullMatrix = Array[Double]()
    for (i <- 0 to rows) rawData ++ rawData.slice(i * columns, (i+1) * columns)
    fullMatrix
  }

  /** convert a fully expanded Array(M*N) (i.e. flattened matrix) into a sparse array */
  override def squishArray(fromArray: Array[Double]): Array[Double] = {
    fromArray.clone()
  }

  /** convert a squished array into a fully expanded one */
  override def expandArray(squished: Array[Double]): Array[Double] = {
    squished.clone()
  }
}

class MatrixMapperImpl(rows: Int, columns: Int, vectorSize: Int) extends MatrixMapper {
  val toMatrix: Array[(Int, Int)] = Array.fill(vectorSize)((-1, -1))

  def addMap(row: Int, col: Int, arrayIndex: Int) : Unit = {
    toMatrix(arrayIndex) = (row, col)
  }
  def convert(fromArray: Array[Double]) : RealMatrix = {
    var realMatrix = new BlockRealMatrix(rows, columns)
    for (i <- fromArray.indices) {
      val (row, col) = toMatrix(i)
      realMatrix.setEntry(row, col, fromArray(i))
    }
    realMatrix
  }
  def convert(fromMatrix: RealMatrix) : Array[Double] = {
    var array = Array.fill(vectorSize)(0.0d)
    for (i <- array.indices) {
      val (row, col) = toMatrix(i)
      array(i) = fromMatrix.getEntry(row, col)
    }
    array
  }
  def squishArray(fromArray: Array[Double]) : Array[Double] = {
    var toArray = Array.fill(vectorSize)(0.0)
    for (i <- toArray.indices) {
      val (row, col) = toMatrix(i)
      toArray(i) = fromArray((row * columns) + col)
    }
    toArray
  }
  /** and convert a squished to a fully expanded array */
  def expandArray(squished: Array[Double]) : Array[Double] = {
    var toArray = Array.fill(rows * columns)(0.0d)
    for (i <- squished.indices) {
      val (row, col) = toMatrix(i)
      toArray((row * columns) + col) = squished(i)
    }
    toArray
  }

}
