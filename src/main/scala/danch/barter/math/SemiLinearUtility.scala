package danch.barter.math

import danch.barter.math.MathUtils.{elementMult, scalarPower, sumRows}
import org.apache.commons.math3.linear.RealMatrix

/** Utility function that is convex up in volumes and linear in coefficients */
class SemiLinearUtility(val volumeDecayFactor: Double, val coefficientScale: Double) extends UtilityFunction {
  override def apply(volumes: RealMatrix, coefficients: RealMatrix): (Array[Double], RealMatrix) = {
    val scaledVolumes =  scalarPower(volumes, volumeDecayFactor)
    val scaledCoefficients = coefficients.scalarMultiply(coefficientScale)
    val product = elementMult(scaledVolumes, scaledCoefficients)
    (sumRows(product), product)
  }
}
