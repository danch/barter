package danch.barter.math

import org.apache.commons.math3.linear.RealMatrix

trait UtilityFunction {
  def apply(volumes: RealMatrix, coefficients: RealMatrix) : (Array[Double], RealMatrix)
}
